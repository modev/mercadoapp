import React from 'react';
import {
  Route,
  Switch,
  HashRouter,
} from 'react-router-dom';
import {
  CSSTransition,
  TransitionGroup
} from 'react-transition-group';
import './App.css';
import Uno from "./pages/Uno";
import Dos from "./pages/Dos";
import Main from "./pages/Main";
import Galeria from "./pages/Galeria";
import End from "./pages/End";

const supportsHistory = 'pushState' in window.history;

function App() {
  return (
      <HashRouter forceRefresh={!supportsHistory}>
        <div>
          <main>
            <Route
                render={({ location }) => {
                  const { pathname } = location;
                  return (
                      <TransitionGroup>
                        <CSSTransition
                            key={pathname}
                            classNames="page"
                            timeout={{
                              enter: 1000,
                              exit: 1000,
                            }}
                        >
                          <Route
                              location={location}
                              render={() => (
                                  <Switch>
                                    <Route
                                        exact
                                        path="/"
                                        component={Uno}
                                    />
                                    <Route
                                        path="/dos"
                                        component={Dos}
                                    />
                                    <Route
                                        path="/main"
                                        component={Main}
                                    />
                                    <Route
                                        path="/galeria"
                                        component={Galeria}
                                    />
                                    <Route
                                        path="/app"
                                        component={End}
                                    />
                                    
                                  </Switch>
                              )}
                          />
                        </CSSTransition>
                      </TransitionGroup>
                  );
                }}
            />
          </main>
        </div>
      </HashRouter>
  );
}

export default App;
