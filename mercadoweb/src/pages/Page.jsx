import React from 'react';
import { withRouter } from 'react-router-dom';

function Page({
                  children,
                  background,
                  location: {
                      state,
                  },
              }) {
    return (
        <div
            className={`page ${state && state.prev?"page--prev":""}`}
            style={{background}}
        >
            {children}
        </div>
    );
}

export default withRouter(Page);