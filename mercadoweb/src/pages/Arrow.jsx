import React from 'react';
import Link from 'react-router-dom/Link';

function Arrow({path}) {
    return (
        <Link to={path}>
            <i className="arrow-down"></i>
        </Link>
    );
}

export default Arrow;