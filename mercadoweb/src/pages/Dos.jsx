import React from 'react';
import { Link } from 'react-router-dom'
import Page from "./Page";
import Completo from "../assets/images/Completo.gif";
import Video from "../assets/images/Comenzar.mp4";
import transparente from "../assets/images/transparente2.png";

export default function Dos() {
    return (
        <Page>
            <video  poster={transparente}   preload="none" controls={false} src={Video} autoPlay={true} loop={true}/>
            <div className="btnStart">
                <Link
                    to={{
                        pathname: "/main",
                        state: { prev: true }
                    }}>
                    <button className={"center start"}>Comenzar</button>
                </Link>
            </div>
        </Page>
    );
}