import React from 'react';
import { Link,Redirect } from 'react-router-dom'
import Page from './Page';
import Image from "../assets/images/imgask.png"
import Sponsor from "../assets/images/Logos-Patrocinadores.png"
import FlechaArriba from "../assets/images/Flecha-Arriba.gif"
import Video from "../assets/images/Eleccion.mp4";
import {opciones} from '../constants.js';
//Variables de las animaciones; que coincidan con los nombres
import Bck1 from "../assets/images/Btn-Splash-Mov-A.gif"
import Bck2 from "../assets/images/Btn-Splash-Mov-B.gif"
import Bck3 from "../assets/images/Btn-Splash-Mov-C.gif"
import Bck4 from "../assets/images/Btn-Splash-Mov-D.gif"
import Bck5 from "../assets/images/Btn-Splash-Mov-E.gif"
import Bck6 from "../assets/images/Btn-Splash-Mov-F.gif"
import Bck7 from "../assets/images/Btn-Splash-Mov-G.gif"
import Bck8 from "../assets/images/Btn-Splash-Mov-H.gif"
import Bck9 from "../assets/images/Btn-Splash-Mov-I.gif"
import Bck10 from "../assets/images/Btn-Splash-Mov-J.gif"
import Bck11 from "../assets/images/Btn-Splash-Mov-K.gif"
import transparente from "../assets/images/transparente2.png";

class Main extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            selected:false,
            selectedKey:false
        }
        this.animations = [Bck1,Bck2,Bck3,Bck4,Bck5,Bck6,Bck7,Bck8,Bck9,Bck10,Bck11];
        this.opciones = opciones;
    }

    choose = (choose,key) =>{
        var self = this;
        this.setState({
            selected:choose,
            selectedKey:key
        }
        )

        setTimeout(function(){
            self.setState({
                siguientePagina: true
            }
            )           
        },2000)

    }

    componentDidMount() {
        this.opciones = shuffleArray(this.opciones);

    }    
    
    render() {
        if (this.state.siguientePagina === true) {
            return <Redirect to={{
                pathname: "/galeria",
                state: { prev: true, category: this.state.selectedKey }
            }} />
          }
        const style = {
            background: `url(${this.animations[Math.floor(Math.random()*this.animations.length)]})`,
            backgroundSize: "111% 224%",
            backgroundPosition: "center"
        };
        return (
            <Page>
                <video  poster={transparente}   src={Video} autoPlay={true} loop={true}/>
                {/* <img src={Image}/> */}
                <div className="scrollQuestions">
                    {
                        this.opciones.map((option,key)=>{
                            return <div key={key} style={this.state.selected === option.category?style:{}} className={`cuadro ${this.state.selected === option.category?"choose":""}`} onClick={event => this.choose(option.category,key)}>{option.name}</div>
                        })
                    }
                </div>
                <img src={Sponsor} style={{ marginTop: "25%", position:"relative" }}/>

            </Page>)
    }

}

function shuffleArray(array) {

    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
  
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
  
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }

    return array;
}

export default Main