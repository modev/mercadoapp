import React from 'react';
import Page from './Page';
import { Link } from 'react-router-dom'
import Video from "../assets/images/Cierre.mp4";
import transparente from "../assets/images/transparente2.png";

export default function End() {
    return (
        <Page>
            <Link to={"/"}>
            <video  poster={transparente}   src={Video} autoPlay={true} />
            {/* <img src={Image}/>
            <p>Compra rapido y facil descargando la App</p>
            <div>QR</div>
            <img src={Logo}/> */}
            </Link>
        </Page>
    );
}