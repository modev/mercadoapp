import React from 'react';
import Page from './Page';
import Sponsor from "../assets/images/Logos-Patrocinadores.png";
import { Link } from 'react-router-dom'
import {withRouter} from "react-router-dom";
import {opciones} from '../constants.js';
import transparente from "../assets/images/transparente2.png";

class Galeria extends React.Component {
    constructor(props){
        super(props);

        }

    componentDidMount() {
        
        console.log(
            this.props.location.state.category,
            opciones[parseInt(this.props.location.state.category)].video
            );

        setTimeout(mostrarbtn, 9000)


        //    this.refs.vidRef.play();

    }


    render() {
        return (
            <Page>
                
                <video  poster={transparente}   ref="vidRef" src={opciones[parseInt(this.props.location.state.category)].video} autoPlay="autoplay" muted></video>
               
                {/* <p>En Mercado Libre tienes mas de <b>897</b> zapatos para seguir poniendo de moda a tu moda</p>
                <p>Compra los tuyos con total libertad en nuestras tiendas de moda oficiales</p> */}
                {/* <img src={Sponsor}/> */}
                <div className="btnStart">
                <Link
                    to={{
                        pathname: "/app",
                        state: { prev: true }
                    }}>
                    <button id="botonGaleria" className="btnGaleria" className={"center start"} >Continuar</button>
                </Link>
                </div>
            </Page>
        );
    }
}

function mostrarbtn(){
   
    let botonGaleria = document.getElementById('botonGaleria');
    if (botonGaleria)
    botonGaleria.style.display='block';
}



export default withRouter(Galeria)