import Video1 from "./assets/images/01 Zapatos.mp4";
import Video2 from  "./assets/images/02 Tenis.mp4";
import Video3 from  "./assets/images/09 Gafas.mp4";
import Video4 from  "./assets/images/04 Accesorios.mp4";
import Video5 from  "./assets/images/11 Relojes.mp4";
import Video6 from "./assets/images/05 Chaquetas.mp4";
import Video7 from  "./assets/images/08 Faldas.mp4";
import Video8 from  "./assets/images/03 Pantalones.mp4";
import Video9 from  "./assets/images/07 Camisas.mp4";
import Video10 from "./assets/images/10 Maquillaje.mp4";
import Video12 from "./assets/images/06 Ropa Deportiva.mp4"


export const opciones = [
    {category:1,video:Video1,name:"Zapatos, uno por cada look."},
    {category:2,video:Video2,name:"Estrenar tenis debería ser como el desayuno, obligatorio."},
    {category:3,video:Video3,name:"Nunca son demasiadas gafas."},
    {category:4,video:Video4,name:"Soy de los que siempre dice sí cuando se trata de accesorios."},
    {category:5,video:Video5,name:"Un reloj nunca puede faltar en mi estilo."},
    {category:6,video:Video6,name:"Chaquetas, Chalecos y Buzos; los aliados perfectos de mis pintas para el clima bipolar."},
    {category:7,video:Video7,name:"Faldas y vestidos, el largo no importa, siempre las uso"},
    {category:8,video:Video8,name:"Pantalones, Jeans y Shorts; algo que no puede faltar en mi closet."},
    {category:9,video:Video9,name:"Camisas, Camisetas y Blusas; son básicas para cada día."},
    {category:10,video:Video10,name:"Te pueden copiar la ropa, pero nunca el maquillaje."},
    {category:12,video:Video12,name:"Ropa deportiva, mi look de todos los días."},
]
