###Mercadolibre fashion category selection

This is a project based on cordova but Its www folder
is created by create-react-app that lives in the mercadoweb subfolder


**Instalación**
```bash
$ npm i
```

### Desarrollo 
Esta libreria se utiliza para mantener información global en la aplicación.

**Instalación**
```bash
$ bower install ngstorage
```

### Compilación para producción 

-- Aumentar versión en config.xml


**Pasos Para Compilar en Android 

*Build unsigned APK 
cordova build --release android

*Sign the APK
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./mocion.keystore ./platforms/android/build/outputs/apk/android-release-unsigned.apk mocion 

password de mocion.keystore: mocion.2040


*order inside the apk

el comando zipalign esta dentro del directorio de instalacion del framwork de android (por si no esta de uso global)

/Users/david/Library/Android/sdk/build-tools/28.0.3/zipalign -v 4 ./platforms/android/build/outputs/apk/android-release-unsigned.apk ./app.apk


|| MAC de JUAN 

jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore ./mocion.keystore /Users/david/js/pfs-events/platforms/android/build/outputs/apk/release/android-release-unsigned.apk mocion

el zip align esta dentro del directorio de instalacion del framwork de android (por si no esta de uso global)

/Users/david/Library/Android/sdk/build-tools/28.0.3/zipalign -v 4 /Users/david/js/pfs-events/platforms/android/build/outputs/apk/release/android-release-unsigned.apk ./app.apk





Pasos Para Compilar en IOS 

1. gulp --cordova 'platform add ios'
2. pod install o sudo gem install cocoapods
3. gulp --cordova 'build ios'